package com.dedi.airyrecipe.ui

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.paging.PagedList
import com.dedi.airyrecipe.data.repository.NetworkState
import com.dedi.airyrecipe.data.vo.Recipe
import com.dedi.airyrecipe.data.vo.RecipeX
import io.reactivex.disposables.CompositeDisposable

class RecipeViewModel(private val repository: RecipeRepository): ViewModel() {
    private val compositeDisposable = CompositeDisposable()

    val  recipePagedList : LiveData<PagedList<RecipeX>> by lazy {
        repository.fetchLivePagedList(compositeDisposable)
    }

    val  networkState : LiveData<NetworkState> by lazy {
        repository.getNetworkState()
    }

    fun listIsEmpty(): Boolean {
        return recipePagedList .value?.isEmpty() ?: true
    }


    override fun onCleared() {
        super.onCleared()
        compositeDisposable.dispose()
    }
}