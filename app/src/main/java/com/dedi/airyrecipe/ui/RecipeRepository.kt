package com.dedi.airyrecipe.ui

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import com.dedi.airyrecipe.data.api.DBInterface
import com.dedi.airyrecipe.data.repository.DataSourceFactory
import com.dedi.airyrecipe.data.repository.NetworkState
import com.dedi.airyrecipe.data.repository.RecipeDataSource
import com.dedi.airyrecipe.data.vo.Recipe
import com.dedi.airyrecipe.data.vo.RecipeX
import io.reactivex.disposables.CompositeDisposable

class RecipeRepository (private val apiService : DBInterface) {

    lateinit var recipeDataSourceFactory: DataSourceFactory
    lateinit var recipePagedList: LiveData<PagedList<RecipeX>>

//    fun fetchData(compositeDisposable: CompositeDisposable):LiveData<RecipeX>{
//        networkDataSource = NetworkDataSource(apiService, compositeDisposable)
//        networkDataSource.fetchData()
//
//        return networkDataSource.downloadResponse
//    }


//    fun networkState():LiveData<NetworkState>{
//        return networkDataSource.networkState
//    }

    fun fetchLivePagedList (compositeDisposable: CompositeDisposable) : LiveData<PagedList<RecipeX>> {
        recipeDataSourceFactory= DataSourceFactory(apiService, compositeDisposable)

        val config = PagedList.Config.Builder()
            .setEnablePlaceholders(false)
//            .setPageSize(POST_PER_PAGE)
            .build()

        recipePagedList = LivePagedListBuilder(recipeDataSourceFactory, config).build()

        return recipePagedList
    }

    fun getNetworkState(): LiveData<NetworkState> {
        return Transformations.switchMap<RecipeDataSource, NetworkState>(
            recipeDataSourceFactory.recipeLiveDataSource, RecipeDataSource::networkState)
    }
}