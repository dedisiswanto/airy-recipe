package com.dedi.airyrecipe.ui

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.dedi.airyrecipe.R
import com.dedi.airyrecipe.data.repository.NetworkState
import com.dedi.airyrecipe.data.vo.RecipeX
import kotlinx.android.synthetic.main.list_item.view.*
import kotlinx.android.synthetic.main.network_state_item.view.*

class RecipeListAdapter (public val context: Context) : PagedListAdapter<RecipeX, RecyclerView.ViewHolder>(MovieDiffCallback())  {

    val RECIPE_VIEW_TYPE = 1
    val NETWORK_VIEW_TYPE = 2

    private var networkState: NetworkState? = null


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val view: View

        if (viewType == RECIPE_VIEW_TYPE ) {
            view = layoutInflater.inflate(R.layout.list_item, parent, false)
            return ItemViewHolder(view)
        } else {
            view = layoutInflater.inflate(R.layout.network_state_item, parent, false)
            return NetworkStateItemViewHolder(view)
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (getItemViewType(position) == RECIPE_VIEW_TYPE ) {
            (holder as ItemViewHolder).bind(getItem(position))
        }
        else {
            (holder as NetworkStateItemViewHolder).bind(networkState)
        }
    }


    private fun hasExtraRow(): Boolean {
        return networkState != null && networkState != NetworkState.LOADED
    }

    override fun getItemCount(): Int {
        return super.getItemCount() + if (hasExtraRow()) 1 else 0
    }

    override fun getItemViewType(position: Int): Int {
        return if (hasExtraRow() && position == itemCount - 1) {
            NETWORK_VIEW_TYPE
        } else {
            RECIPE_VIEW_TYPE
        }
    }




    class MovieDiffCallback : DiffUtil.ItemCallback<RecipeX>() {
        override fun areItemsTheSame(oldItem: RecipeX, newItem: RecipeX): Boolean {
            return oldItem.name == newItem.name
        }

        override fun areContentsTheSame(oldItem: RecipeX, newItem: RecipeX): Boolean {
            return oldItem == newItem
        }

    }


    class ItemViewHolder (view: View) : RecyclerView.ViewHolder(view) {

        fun bind(recipe: RecipeX?) {
            itemView.cv_title.text = recipe?.name
            itemView.cv_release_by.text =  "By : "+recipe?.creator
            itemView.cv_desc.text =  recipe?.review
            itemView.cv_rating.text = recipe?.rating.toString()

            if (recipe != null) {
                if (recipe?.rating < 4.0){
                    itemView.lay_rating.visibility = View.GONE
                }
//                if (recipe?.name.contains("Chicken")){
//                    itemView.card_view.visibility = View.VISIBLE
//                }else{
//                    itemView.card_view.visibility = View.GONE
//                }
            }

            val movieURL = recipe?.image
            Glide.with(itemView.context)
                .load(movieURL)
                .into(itemView.cv_iv_poster);


        }

    }

    class NetworkStateItemViewHolder (view: View) : RecyclerView.ViewHolder(view) {

        fun bind(networkState: NetworkState?) {
            if (networkState != null && networkState == NetworkState.LOADING) {
                itemView.progress_bar_item.visibility = View.VISIBLE;
            }
            else  {
                itemView.progress_bar_item.visibility = View.GONE;
            }

            if (networkState != null && networkState == NetworkState.ERROR) {
                itemView.error_msg_item.visibility = View.VISIBLE;
                itemView.error_msg_item.text = networkState.msg;
            }
            else if (networkState != null && networkState == NetworkState.ENDOFLIST) {
                itemView.error_msg_item.visibility = View.VISIBLE;
                itemView.error_msg_item.text = networkState.msg;
            }
            else {
                itemView.error_msg_item.visibility = View.GONE;
            }
        }
    }


    fun setNetworkState(newNetworkState: NetworkState) {
        val previousState = this.networkState
        val hadExtraRow = hasExtraRow()
        this.networkState = newNetworkState
        val hasExtraRow = hasExtraRow()

        if (hadExtraRow != hasExtraRow) {
            if (hadExtraRow) {                             //hadExtraRow is true and hasExtraRow false
                notifyItemRemoved(super.getItemCount())    //remove the progressbar at the end
            } else {                                       //hasExtraRow is true and hadExtraRow false
                notifyItemInserted(super.getItemCount())   //add the progressbar at the end
            }
        } else if (hasExtraRow && previousState != newNetworkState) { //hasExtraRow is true and hadExtraRow true and (NetworkState.ERROR or NetworkState.ENDOFLIST)
            notifyItemChanged(itemCount - 1)       //add the network message at the end
        }

    }
}