package com.dedi.airyrecipe.data.repository

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.paging.PageKeyedDataSource
import com.dedi.airyrecipe.data.api.DBInterface
import com.dedi.airyrecipe.data.vo.RecipeX
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class RecipeDataSource (private val apiService : DBInterface, private val compositeDisposable: CompositeDisposable)
    : PageKeyedDataSource<Int, RecipeX>() {

//    private var page = FIRST_PAGE

    val networkState: MutableLiveData<NetworkState> = MutableLiveData()


    override fun loadInitial(params: LoadInitialParams<Int>, callback: LoadInitialCallback<Int, RecipeX>) {

        networkState.postValue(NetworkState.LOADING)

        compositeDisposable.add(
            apiService.getRecipe()
                .subscribeOn(Schedulers.io())
                .subscribe(
                    {
                        callback.onResult(it.recipe, null, null)
                        networkState.postValue(NetworkState.LOADED)
                    },
                    {
                        networkState.postValue(NetworkState.ERROR)
                        Log.e("DataSource", it.message)
                    }
                )
        )
    }

    override fun loadAfter(params: LoadParams<Int>, callback: LoadCallback<Int, RecipeX>) {
        networkState.postValue(NetworkState.LOADING)

        compositeDisposable.add(
            apiService.getRecipe()
                .subscribeOn(Schedulers.io())
                .subscribe(
                    {
//                        if(it.totalPages >= params.key) {
//                            callback.onResult(it.movieList, params.key+1)
//                            networkState.postValue(NetworkState.LOADED)
//                        }
//                        else{
                            networkState.postValue(NetworkState.ENDOFLIST)
//                        }
                    },
                    {
                        networkState.postValue(NetworkState.ERROR)
                        Log.e("MovieDataSource", it.message)
                    }
                )
        )
    }

    override fun loadBefore(params: LoadParams<Int>, callback: LoadCallback<Int, RecipeX>) {

    }
}