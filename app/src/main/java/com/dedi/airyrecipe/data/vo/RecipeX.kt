package com.dedi.airyrecipe.data.vo


import com.google.gson.annotations.SerializedName

data class RecipeX(
    @SerializedName("creator")
    val creator: String,
    @SerializedName("image")
    val image: String,
    @SerializedName("ingredients")
    val ingredients: List<String>,
    @SerializedName("name")
    val name: String,
    @SerializedName("rating")
    val rating: Double,
    @SerializedName("review")
    val review: String
)