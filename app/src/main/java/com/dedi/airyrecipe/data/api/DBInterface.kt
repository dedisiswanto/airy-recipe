package com.dedi.airyrecipe.data.api

import com.dedi.airyrecipe.data.vo.Recipe
import com.dedi.airyrecipe.data.vo.RecipeX
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path

interface DBInterface {
//    https://api.myjson.com/bins/171v5n

    @GET("bins/171v5n")
    fun getRecipe():Single<Recipe>

}