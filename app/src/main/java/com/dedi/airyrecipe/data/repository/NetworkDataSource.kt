package com.dedi.airyrecipe.data.repository

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.dedi.airyrecipe.data.api.DBInterface
import com.dedi.airyrecipe.data.vo.Recipe
import com.dedi.airyrecipe.data.vo.RecipeX
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import java.lang.Exception

class NetworkDataSource (private val apiService : DBInterface, private val compositeDisposable: CompositeDisposable){

    private val _networkState = MutableLiveData<NetworkState>()
    val networkState: LiveData<NetworkState>
        get() = _networkState

    private val _downloadedResponse = MutableLiveData<Recipe>()
    val downloadResponse : LiveData<Recipe>
        get() = _downloadedResponse

    fun fetchData(){

        _networkState.postValue(NetworkState.LOADING)

        try {
            compositeDisposable.add(
                apiService.getRecipe()
                    .subscribeOn(Schedulers.io())
                    .subscribe(
                        {
                            _downloadedResponse.postValue(it)
                            _networkState.postValue(NetworkState.LOADED)
                        },
                        {
                            _networkState.postValue(NetworkState.ERROR)
                            Log.e("DataSource",it.message)
                        })
            )

        } catch (e:Exception){
            Log.e("DataSource",e.message)
        }

    }

}