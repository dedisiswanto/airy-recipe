package com.dedi.airyrecipe.data.repository

import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource
import com.dedi.airyrecipe.data.api.DBInterface
import com.dedi.airyrecipe.data.vo.RecipeX
import io.reactivex.disposables.CompositeDisposable

class DataSourceFactory (private val apiService : DBInterface, private val compositeDisposable: CompositeDisposable)
    : DataSource.Factory<Int, RecipeX>()  {

    val recipeLiveDataSource =  MutableLiveData<RecipeDataSource>()

    override fun create(): DataSource<Int, RecipeX> {
        val movieDataSource = RecipeDataSource(apiService,compositeDisposable)

        recipeLiveDataSource.postValue(movieDataSource)
        return movieDataSource
    }
}