package com.dedi.airyrecipe.data.vo


import com.google.gson.annotations.SerializedName

data class Recipe(
    @SerializedName("recipe")
    val recipe: List<RecipeX>
)